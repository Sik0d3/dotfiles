#!/usr/bin/dash

# Export User Environment Variables
[ -f ~/.config/.shenv ] && . ~/.config/.shenv

# IF no Display AND IF TTY = 1 Then run a Wayland Session OR log into zsh shell.
[ ! $DISPLAY ]  &&  [ $XDG_VTNR = 1 ] && exec dbus-run-session $DESKTOP || exec zsh
